check-manifest (0.50-1) unstable; urgency=medium

  * d/watch: Modernize.
  * New upstream version 0.50
  * d/control: Remove bzr from B-D (see bug #1099219).
  * d/p/0002-Skip-bzr-tests.patch: Skip bzr tests.
    For more information, see bug #1099219.
  * d/control: Bump Standards-Version to 4.7.2; no changes needed.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Sat, 01 Mar 2025 16:40:05 -0500

check-manifest (0.49-3) unstable; urgency=medium

  * Team upload.
  * Remove dependency on old mock
  * Use new dh-sequence-python3

 -- Alexandre Detiste <tchet@debian.org>  Thu, 28 Mar 2024 23:28:06 +0100

check-manifest (0.49-2) unstable; urgency=medium

  * Team upload.
  * Remove python3-pep517 from build-depends, deprecated and no longer needed

 -- Scott Kitterman <scott@kitterman.com>  Wed, 02 Aug 2023 09:40:34 -0400

check-manifest (0.49-1) unstable; urgency=medium

  * New upstream version 0.49 (Closes: #1023801, #1026518)
  * d/control: Bump Standards-Version to 4.6.2; no changes needed.
  * d/copyright: Update year and email address.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Tue, 20 Dec 2022 14:52:47 -0500

check-manifest (0.46-2) unstable; urgency=medium

  * Team upload.
  * Use pytest instead of nose. (Closes: #1018328)
  * Set Standards-Version to 4.6.1.

 -- Emanuele Rocca <ema@debian.org>  Sun, 25 Sep 2022 12:40:50 +0200

check-manifest (0.46-1) unstable; urgency=medium

  * Team upload
  * New upstream version 0.46
  * d/p/0001-Make-sure-we-create-HOME-.config-breezy-before-testi.patch:
    Add forwarded tag.

 -- Arthur Diniz <arthurbdiniz@gmail.com>  Tue, 19 Jan 2021 05:00:36 -0300

check-manifest (0.45-2) unstable; urgency=medium

  * d/control: Add python3-build as a B-D. (Closes: #975929)
  * d/control: Bump Standards-Version to 4.5.1.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Sun, 06 Dec 2020 18:12:47 -0500

check-manifest (0.45-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sergio Durigan Junior ]
  * New upstream version 0.45
  * d/control: Add test dependencies to Build-Depends.
  * d/rules: Set up tests during to run during build time.
  * d/control: Add Rules-Requires-Root: no.
  * d/tests/*: Revamp autopkgtest and fix it.
    Previously, we were (naively) testing things using tox, which ended up
    running pip, and therefore not testing our package.  The autopkgtest
    has now been rewritten and improved, so that everything is properly
    tested.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Wed, 11 Nov 2020 00:39:04 -0500

check-manifest (0.42-1) unstable; urgency=medium

  * d/watch: Fetch sources from pypi.debian.net instead of github.com.
    This is better because we end up getting more files that are needed to
    properly build and test everything, like tox.ini.
  * New upstream version 0.42
  * d/control: Bump debhelper-compat to 13.
  * d/control: Bump Standards-Version to 4.5.0.
  * Update dep8 tests to use tox.
    Upstream recommends using tox to run the tests, so that's what we're
    doing.
  * d/control: Remove test dependencies.
    We run the tests using dep8, not during build time.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Sun, 02 Aug 2020 12:52:44 -0400

check-manifest (0.40-1) unstable; urgency=medium

  * New upstream version 0.40.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Sat, 19 Oct 2019 20:11:56 -0400

check-manifest (0.39-2) unstable; urgency=medium

  * Bump version for source-only upload.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Tue, 08 Oct 2019 00:02:25 -0400

check-manifest (0.39-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * d/control: Remove ancient X-Python3-Version field.

  [ Sergio Durigan Junior ]
  * New upstream version 0.39.
  * Depend on 'brz' (breezy) instead of 'bzr' (Bazaar).
    Bazaar is being deprecated, and breezy is the replacement for it.  We
    now depend on 'brz' (breezy) for our testing purposes.
  * Build-Depend on python3-toml.
  * Make sure we create $HOME/.config/breezy when testing breezy.
    (Closes: #941558)
  * Bump Standards-Version to 4.4.1.
  * Bump compat version to 12.
  * Cleanup d/watch file.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Wed, 02 Oct 2019 10:10:14 -0400

check-manifest (0.37-1) unstable; urgency=medium

  * Add d/tests/control.
    Because the package name doesn't start with "python*-", debci doesn't
    automatically run the regular Python tests.  Therefore, we need to
    manually provide the scripts necessary to do it.
  * Update my e-mail address and bump Standards-Version.
  * New upstream version 0.37
  * Remove Testsuite: field from d/control.

 -- Sergio Durigan Junior <sergiodj@debian.org>  Tue, 17 Apr 2018 14:37:35 -0400

check-manifest (0.36-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Sergio Durigan Junior ]
  * Re-enable tests.
    I made a mistake when packaging and assumed that the tests required
    internet connection.  They don't.  Therefore, we can re-enable them.
    This also adds the necessary Build-Depends for the tests.
  * Bump d/compat version to 11.

 -- Sergio Durigan Junior <sergiodj@sergiodj.net>  Sat, 17 Feb 2018 12:31:02 -0500

check-manifest (0.36-1) unstable; urgency=medium

  * Initial release (Closes: #734117)

 -- Sergio Durigan Junior <sergiodj@sergiodj.net>  Thu, 28 Dec 2017 01:47:13 -0500
